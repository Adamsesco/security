import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/sec_app.dart';
import 'package:security_company_app/app/common/themes/sec_theme.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();
}

class AppLaunched extends AppEvent {
  const AppLaunched();

  @override
  List<Object> get props => [];
}

class AppStarted extends AppEvent {
  final SecApp app;
  final SecTheme theme;

  const AppStarted({@required this.app, @required this.theme});

  @override
  List<Object> get props => [theme];
}
