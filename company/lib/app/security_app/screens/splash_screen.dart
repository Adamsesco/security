import 'package:flutter/material.dart';
import 'package:security_company_app/app/common/consts/palette.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Palette.sanMarino, Palette.cello],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Hero(
          tag: 'logo',
          child: Image(
            image: AssetImage('assets/images/shield.png'),
          ),
        ),
      ),
    );
  }
}
