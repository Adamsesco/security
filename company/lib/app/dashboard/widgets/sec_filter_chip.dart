import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';

class SecFilterChip extends StatelessWidget {
  final bool selected;
  final void Function() onTap;
  final Widget child;

  SecFilterChip({this.selected, this.onTap, this.child});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(14).toDouble(),
          vertical: ScreenUtil().setHeight(8).toDouble(),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(17.5),
          color: selected ? Palette.sanMarino : Colors.white,
          boxShadow: selected
              ? [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.08),
                    offset: Offset(0, 2),
                    blurRadius: 4,
                    spreadRadius: 0,
                  ),
                ]
              : [],
        ),
        child: child ?? Container(),
      ),
    );
  }
}
