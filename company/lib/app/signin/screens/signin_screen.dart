import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/signin/bloc/bloc.dart';

class SigninScreen extends StatefulWidget {
  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  SignInBloc _signInBloc;
  String _email;
  String _password;
  int _type = 0;

  final FocusNode _passwordFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();

    _signInBloc = context.bloc<SignInBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignInBloc, SignInState>(
      listener: (BuildContext context, SignInState signInState) {
        if (signInState is SignInFailure) {
          final message = signInState.errorMessage;

          showCupertinoDialog(
            context: context,
            builder: (context) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text(message),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (signInState is SignInSuccess) {
          final user = signInState.user;

          if (user is Client) {
            Router.navigator.pushNamedAndRemoveUntil(
                Router.clientDashboardScreen, (_) => false);
          } else if (user is Company || user is Bodyguard) {
            Router.navigator.pushNamedAndRemoveUntil(
                Router.companyDashboardScreen, (_) => false);
          }
        }
      },
      child: BlocBuilder<AppBloc, AppState>(
        builder: (BuildContext context, AppState state) {
          final appState = state as AppLoadSuccess;

          return BlocBuilder<SignInBloc, SignInState>(
            builder: (context, signInState) {
              return Scaffold(
                body: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Palette.sanMarino, Palette.cello],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: SafeArea(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                        Row(
                          children: <Widget>[
                            SizedBox(
                                width: ScreenUtil().setWidth(29).toDouble()),
                            SecBackButton(),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(19).toDouble()),
                        Padding(
                          padding: EdgeInsetsDirectional.only(
                            start: ScreenUtil().setWidth(39).toDouble(),
                          ),
                          child: Text(
                            'Sign in',
                            style: appState.theme.titleStyle,
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(28).toDouble()),
                        Expanded(
                          child: Container(
                            constraints: BoxConstraints(
                              minWidth: double.infinity,
                            ),
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(31).toDouble(),
                              vertical: ScreenUtil().setHeight(34).toDouble(),
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                            ),
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: AlignmentDirectional.topStart,
                                  child: Text(
                                    'Welcome Back',
                                    style: appState.theme.headStyle,
                                  ),
                                ),
                                SizedBox(
                                    height:
                                        ScreenUtil().setHeight(5).toDouble()),
                                Align(
                                  alignment: AlignmentDirectional.topStart,
                                  child: Text(
                                    'Sign in to continue',
                                    style: appState.theme.subheadStyle,
                                  ),
                                ),
                                Spacer(),
                                TextField(
                                  onChanged: (String value) {
                                    _email = value;
                                  },
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(_passwordFocusNode);
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.next,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      bottom: ScreenUtil()
                                          .setHeight(30.5)
                                          .toDouble(),
                                    ),
                                    hintText: 'Email address',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(14).toDouble(),
                                      color: Color(0xFFB8BCC7),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: ScreenUtil()
                                        .setHeight(30.5)
                                        .toDouble()),
                                TextField(
                                  onChanged: (String value) {
                                    _password = value;
                                  },
                                  focusNode: _passwordFocusNode,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      bottom: ScreenUtil()
                                          .setHeight(30.5)
                                          .toDouble(),
                                    ),
                                    hintText: 'Password',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(14).toDouble(),
                                      color: Color(0xFFB8BCC7),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: ScreenUtil()
                                        .setHeight(14.5)
                                        .toDouble()),
                                Align(
                                  alignment: AlignmentDirectional.topEnd,
                                  child: Text(
                                    'Forgot Password ?',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      color: Color(0xFFB8BCC7),
                                    ),
                                  ),
                                ),
                                Spacer(flex: 3),
                                Container(
                                  padding: EdgeInsets.all(
                                      ScreenUtil().setWidth(4).toDouble()),
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF3F3F3),
                                    borderRadius: BorderRadius.circular(25.5),
                                    border: Border.all(
                                      color: Color(0xFFE4E4E4),
                                      width: 1,
                                    ),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              _type = 0;
                                            });
                                          },
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                              vertical: ScreenUtil()
                                                  .setHeight(9)
                                                  .toDouble(),
                                            ),
                                            decoration: BoxDecoration(
                                              color: _type == 0
                                                  ? Color(0xFF212A37)
                                                  : Colors.transparent,
                                              borderRadius:
                                                  BorderRadius.circular(25.5),
                                            ),
                                            alignment: Alignment.center,
                                            child: Text(
                                              'Agent',
                                              style: TextStyle(
                                                fontFamily: 'Rubik',
                                                fontSize: ScreenUtil()
                                                    .setSp(21)
                                                    .toDouble(),
                                                color: _type == 0
                                                    ? Color(0xFFFBF9F7)
                                                    : Color(0xFF212A37),
                                                letterSpacing: -0.3,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      //
                                      //
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              _type = 1;
                                            });
                                          },
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                              vertical: ScreenUtil()
                                                  .setHeight(9)
                                                  .toDouble(),
                                            ),
                                            decoration: BoxDecoration(
                                              color: _type == 1
                                                  ? Color(0xFF212A37)
                                                  : Colors.transparent,
                                              borderRadius:
                                                  BorderRadius.circular(25.5),
                                            ),
                                            alignment: Alignment.center,
                                            child: Text(
                                              'Individual',
                                              style: TextStyle(
                                                fontFamily: 'Rubik',
                                                fontSize: ScreenUtil()
                                                    .setSp(21)
                                                    .toDouble(),
                                                color: _type == 1
                                                    ? Color(0xFFFBF9F7)
                                                    : Color(0xFF212A37),
                                                letterSpacing: -0.3,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Spacer(),
                                SecPrimaryButton(
                                  color: Color(0xFF4361BA),
                                  text: 'Sign in',
                                  isLoading: signInState is SignInInProgress,
                                  onTap: () {
                                    print('Signing in...');
                                    _signInBloc.add(SignInRequested(
                                        email: _email, password: _password));
                                  },
                                ),
//                                Spacer(flex: 3),
                                Visibility(
                                  visible: false,
                                  child: InkWell(
                                    onTap: () {
                                      Router.navigator
                                          .pushNamed(Router.signupScreen);
                                    },
                                    child: RichText(
                                      text: TextSpan(
                                        style: TextStyle(
                                          fontFamily: 'Rubik',
                                          fontSize:
                                              ScreenUtil().setSp(13).toDouble(),
                                          color: Color(0xFFB8BCC7),
                                        ),
                                        text: 'Don\'t have an account ? ',
                                        children: [
                                          TextSpan(
                                            text: 'Sign up',
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xFF4361BA),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
