import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class SignInEvent extends Equatable {
  const SignInEvent();
}

class SignInRequested extends SignInEvent {
  final String email;
  final String password;

  SignInRequested({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];
}