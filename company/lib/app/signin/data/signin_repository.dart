import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/models/language.dart';
import 'package:security_company_app/app/common/models/user.dart';

class SignInRepository {
  final String apiUrl;

  SignInRepository({@required this.apiUrl});

  Future<User> signIn(String email, String password) async {
    await Future.delayed(Duration(seconds: 2));

    if (email == 'client@morocode.com' && password == '123456') {
      final client = Client(
          id: '3',
          firstName: 'Youssef',
          lastName: 'Gougane',
          phoneNumber: '+212668657704',
          language: Language.english,
          available: true,
          notificationsEnabled: true);

      return client;
    } else if (email == 'company@morocode.com' && password == '123456') {
      final company = Company(
          id: '1',
          name: 'Thundersec',
          phoneNumber: '+212668657704',
          language: Language.english,
          available: true,
          notificationsEnabled: true);

      return company;
    } else if (email == 'bodyguard@morocode.com' && password == '123456') {
      final bodyguard = Bodyguard(
        id: '2',
        firstName: 'Brian',
        lastName: 'Norton',
        phoneNumber: '+212668657704',
        language: Language.english,
        available: true,
        notificationsEnabled: true,
      );

      return bodyguard;
    }

    return null;
  }
}
