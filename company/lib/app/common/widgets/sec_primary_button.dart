import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';

class SecPrimaryButton extends StatefulWidget {
  final bool inverted;
  final String text;
  final Widget prefix;
  final Color color;
  final Color disabledColor;
  final TextStyle textStyle;
  final bool enabled;
  final bool isLoading;
  final void Function() onTap;

  SecPrimaryButton(
      {Key key,
      this.inverted = false,
      this.text,
      this.prefix,
      this.color,
      this.disabledColor,
      this.textStyle,
      this.enabled = true,
      this.isLoading = false,
      this.onTap})
      : super(key: key);

  @override
  _SecPrimaryButtonState createState() => _SecPrimaryButtonState();
}

class _SecPrimaryButtonState extends State<SecPrimaryButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.enabled ? widget.onTap : null,
      child: Container(
        constraints: BoxConstraints(
          minWidth: double.infinity,
        ),
        padding: EdgeInsets.symmetric(
          vertical: ScreenUtil().setHeight(13).toDouble(),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(23.5),
          color: widget.enabled
              ? (widget.color ??
                  (widget.inverted ? Colors.transparent : Palette.sanMarino))
              : (widget.disabledColor ?? Palette.mystic),
          border: !widget.inverted || !widget.enabled
              ? null
              : Border.all(
                  color: Colors.white,
                  width: 1,
                ),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            if (widget.prefix != null)
              Positioned.directional(
                textDirection: Directionality.of(context),
                start: ScreenUtil().setWidth(37).toDouble(),
                child: widget.prefix,
              ),
            if (widget.isLoading)
              CupertinoTheme(
                data: CupertinoTheme.of(context)
                    .copyWith(brightness: Brightness.dark),
                child: CupertinoActivityIndicator(),
              ),
            Visibility(
              visible: !widget.isLoading,
              maintainAnimation: true,
              maintainSize: true,
              maintainState: true,
              child: Container(
                child: Text(
                  widget.text ?? '',
                  style: widget.textStyle ??
                      TextStyle(
                        fontFamily: 'Rubik',
                        fontSize: ScreenUtil().setSp(18).toDouble(),
                        fontWeight: FontWeight.w600,
                        color: Palette.springWood,
                      ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
