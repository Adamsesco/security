import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/routes/router.gr.dart';

class SecBackButton extends StatelessWidget {
  final void Function() onTap;

  SecBackButton({this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: onTap ??
          () {
            Router.navigator.pop();
          },
      child: Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(14).toDouble()),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Palette.springWood,
//          boxShadow: [
//            BoxShadow(
//              color: Colors.black.withOpacity(0.07),
//              blurRadius: 6,
//              spreadRadius: 2,
//            ),
//          ],
        ),
        child: Image.asset(
          'assets/images/back-button-icon.png',
          width: ScreenUtil().setWidth(17).toDouble(),
          height: ScreenUtil().setWidth(17).toDouble(),
        ),
      ),
    );
  }
}
