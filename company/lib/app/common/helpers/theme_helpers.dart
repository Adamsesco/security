import 'package:security_company_app/app/common/themes/default_theme.dart';
import 'package:security_company_app/app/common/themes/sec_theme.dart';
import 'package:security_company_app/app/security_app/bloc/app_state.dart';

SecTheme getThemeFromState(AppState appState) {
  if (appState is AppLoadSuccess) {
    return appState.theme;
  } else {
    return DefaultTheme();
  }
}
