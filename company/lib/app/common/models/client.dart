import 'package:security_company_app/app/common/models/language.dart';
import 'package:security_company_app/app/common/models/user.dart';

class Client extends User {
  @override
  String id;

  @override
  String name;

  String firstName;

  String lastName;

  @override
  String phoneNumber;

  @override
  String avatarUrl;

  @override
  Language language;

  @override
  bool available;

  @override
  bool notificationsEnabled;

  Client(
      {this.id,
      this.name,
      this.firstName,
      this.lastName,
      this.phoneNumber,
      this.avatarUrl,
      this.language,
      this.available,
      this.notificationsEnabled});

  @override
  factory Client.fromMap(Map<String, dynamic> map) {
    return Client();
  }

  @override
  factory Client.copyFrom(Client other) {
    return Client(
      id: other.id,
      name: other.name,
      firstName: other.firstName,
      lastName: other.lastName,
      phoneNumber: other.phoneNumber,
      avatarUrl: other.avatarUrl,
      language: other.language,
      available: other.available,
      notificationsEnabled: other.notificationsEnabled,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return null;
  }
}
