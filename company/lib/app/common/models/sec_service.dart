import 'package:flutter/material.dart';

abstract class SecService {
  String get name;
  String get description;
  Widget get icon;
  List<SecService> get subServices;
}
