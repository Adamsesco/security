import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/models/door_supervision_sub_service.dart';
import 'package:security_company_app/app/common/models/manned_security_sub_service.dart';
import 'package:security_company_app/app/common/models/night_watch_security_sub_service.dart';
import 'package:security_company_app/app/common/models/sec_service.dart';
import 'package:security_company_app/app/common/models/static_security_sub_service.dart';

class SecurityGuardService extends SecService {
  @override
  String get description =>
      'At vero eos censes aut odit aut quid percipit aut rerum necessitatibus saepe.';

  @override
  Widget get icon => Image.asset(
        'assets/images/security-guard-icon.png',
        width: ScreenUtil().setWidth(50).toDouble(),
        height: ScreenUtil().setWidth(46).toDouble(),
      );

  @override
  String get name => 'SECURITY GUARD';

  @override
  List<SecService> get subServices => [
    DoorSupervisionSubService(),
    MannedSecuritySubService(),
    StaticSecuritySubService(),
    NightWatchSecuritySubService(),
  ];
}
