import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/sec_service.dart';
import 'package:security_company_app/app/common/themes/sec_theme.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class ServiceCard extends StatelessWidget {
  final int index;
  final SecService service;

  ServiceCard({@required this.index, @required this.service});

  double _getCardHeight(SecTheme theme, TextDirection textDirection) {
    final textSpan = TextSpan(
      text: 'A\nA',
      style: theme.serviceNameStyle,
      children: [
        TextSpan(
          text: 'A\nA\nA',
          style: theme.serviceDescriptionStyle,
        ),
      ],
    );

    final textPainter = TextPainter(
      text: textSpan,
      textDirection: textDirection,
    );
    textPainter.layout();

    return ScreenUtil().setHeight(21 * 2).toDouble() +
        textPainter.height +
        ScreenUtil().setHeight(12).toDouble();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);
      final cardHeight = _getCardHeight(theme, Directionality.of(context));

      return Container(
        constraints: BoxConstraints(
          maxWidth: double.infinity,
        ),
        height: cardHeight,
        margin: EdgeInsets.only(
          right: ScreenUtil().setWidth(16).toDouble(),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(12),
            bottomRight: Radius.circular(12),
          ),
          color: Palette.alabaster,
          boxShadow: [
            BoxShadow(
              color: Palette.dullLavender.withOpacity(0.34),
              offset: Offset(1, 0),
              blurRadius: 3,
            ),
          ],
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              right: 0,
              child: Transform.translate(
                offset: index % 2 == 0
                    ? Offset(ScreenUtil().setWidth(cardHeight).toDouble(), 0)
                    : Offset(ScreenUtil().setWidth(cardHeight).toDouble(),
                        -ScreenUtil().setWidth(cardHeight).toDouble()),
                child: Container(
                  width: ScreenUtil().setWidth(cardHeight * 2).toDouble(),
                  height: ScreenUtil().setWidth(cardHeight * 2).toDouble(),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Palette.sanMarino.withOpacity(0.04),
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 4,
                    child: Container(
                      alignment: Alignment.center,
                      child: service.icon,
                    ),
                  ),
                  Expanded(
                    flex: 6,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          service.name,
                          style: theme.serviceNameStyle,
                        ),
                        SizedBox(height: ScreenUtil().setHeight(12).toDouble()),
                        Text(
                          service.description,
                          style: theme.serviceDescriptionStyle,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.centerLeft,
//                      child: ,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
