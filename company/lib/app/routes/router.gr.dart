// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:security_company_app/app/home/screens/home_screen.dart';
import 'package:security_company_app/app/signin/screens/signin_screen.dart';
import 'package:security_company_app/app/signup/screens/signup_screen.dart';
import 'package:security_company_app/app/intro/screens/intro_screen.dart';
import 'package:security_company_app/app/home/screens/main_screen.dart';
import 'package:security_company_app/app/location/screens/location_screen.dart';
import 'package:security_company_app/app/services/screens/select_service_screen.dart';
import 'package:security_company_app/app/calendar/screens/select_date_time_screen.dart';
import 'package:security_company_app/app/guards_number/screens/guards_number_screen.dart';
import 'package:security_company_app/app/search/screens/search_results_screen.dart';
import 'package:security_company_app/app/dashboard/screens/client_dashboard_screen.dart';
import 'package:security_company_app/app/dashboard/screens/company_dashboard_screen.dart';
import 'package:security_company_app/app/settings/screens/settings_screen.dart';
import 'package:security_company_app/app/notifications/screens/notifications_screen.dart';

class Router {
  static const homeScreen = '/';
  static const signinScreen = '/signin-screen';
  static const signupScreen = '/signup-screen';
  static const introScreen = '/intro-screen';
  static const mainScreen = '/main-screen';
  static const locationScreen = '/location-screen';
  static const selectServiceScreen = '/select-service-screen';
  static const selectDateTimeScreen = '/select-date-time-screen';
  static const guardsNumberScreen = '/guards-number-screen';
  static const searchResultsScreen = '/search-results-screen';
  static const clientDashboardScreen = '/client-dashboard-screen';
  static const companyDashboardScreen = '/company-dashboard-screen';
  static const settingsScreen = '/settings-screen';
  static const notificationsScreen = '/notifications-screen';
  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<Router>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.homeScreen:
        return MaterialPageRoute(
          builder: (_) => HomeScreen(),
          settings: settings,
        );
      case Router.signinScreen:
        return MaterialPageRoute(
          builder: (_) => SigninScreen(),
          settings: settings,
        );
      case Router.signupScreen:
        return MaterialPageRoute(
          builder: (_) => SignupScreen(),
          settings: settings,
        );
      case Router.introScreen:
        return MaterialPageRoute(
          builder: (_) => IntroScreen(),
          settings: settings,
        );
      case Router.mainScreen:
        return MaterialPageRoute(
          builder: (_) => MainScreen(),
          settings: settings,
        );
      case Router.locationScreen:
        return MaterialPageRoute(
          builder: (_) => LocationScreen().wrappedRoute,
          settings: settings,
        );
      case Router.selectServiceScreen:
        return MaterialPageRoute(
          builder: (_) => SelectServiceScreen(),
          settings: settings,
        );
      case Router.selectDateTimeScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => SelectDateTimeScreen(key: typedArgs),
          settings: settings,
        );
      case Router.guardsNumberScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => GuardsNumberScreen(key: typedArgs),
          settings: settings,
        );
      case Router.searchResultsScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => SearchResultsScreen(key: typedArgs),
          settings: settings,
        );
      case Router.clientDashboardScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => ClientDashboardScreen(key: typedArgs),
          settings: settings,
        );
      case Router.companyDashboardScreen:
        return MaterialPageRoute(
          builder: (_) => CompanyDashboardScreen(),
          settings: settings,
        );
      case Router.settingsScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => SettingsScreen(key: typedArgs),
          settings: settings,
        );
      case Router.notificationsScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => NotificationsScreen(key: typedArgs),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}
