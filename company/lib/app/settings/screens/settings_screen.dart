import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/widgets/avatar.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/settings/widgets/settings_item.dart';

class SettingsScreen extends StatefulWidget {
  SettingsScreen({Key key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _isAvailable = true;
  bool _notificationsEnabled = true;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);

      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topRight,
            ),
            color: Palette.mystic,
          ),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                Container(
                  alignment: AlignmentDirectional.topEnd,
                  margin: EdgeInsetsDirectional.only(
                    end: ScreenUtil().setWidth(21).toDouble(),
                  ),
                  child: Text(
                    'SETTINGS',
                    style: theme.screenTitleStyle,
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(15).toDouble()),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(8).toDouble(),
                  ),
                  padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(17).toDouble(),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/images/settings-header-background.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: ScreenUtil().setWidth(36).toDouble()),
                      Avatar(
                        radius: ScreenUtil().setWidth(66 / 2).toDouble(),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(17).toDouble()),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'John Doe',
                            style: theme.settingsNameStyle,
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(4).toDouble()),
                          Text(
                            'Member since Mar 2020',
                            style: theme.settingsMemberSinceStyle,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(31).toDouble()),
                Expanded(
                  child: ListView(
                    children: <Widget>[
//                      SettingsItem(
//                        icon:
//                            Image.asset('assets/images/availability-icon.png'),
//                        text: 'Availability',
//                        hasSwitch: true,
//                        isActive: _isAvailable,
//                        onChanged: (bool value) {
//                          setState(() {
//                            _isAvailable = value;
//                          });
//                        },
//                      ),
//                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      SettingsItem(
                        icon: Image.asset('assets/images/name-icon.png'),
                        text: 'Name',
                      ),
                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      SettingsItem(
                        icon: Image.asset('assets/images/phone-icon.png'),
                        text: 'Phone',
                      ),
                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      SettingsItem(
                        icon: Image.asset('assets/images/password-icon.png'),
                        text: 'Password',
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 0.2,
                        color: Palette.silverChalice,
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      SettingsItem(
                        icon: Image.asset('assets/images/languages-icon.png'),
                        text: 'Languages',
                      ),
                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      InkWell(
                        onTap: () {
                          Router.navigator
                              .pushNamed(Router.notificationsScreen);
                        },
                        child: SettingsItem(
                          icon: Image.asset(
                              'assets/images/notifications-icon.png'),
                          text: 'Notifications',
                          hasSwitch: true,
                          isActive: _notificationsEnabled,
                          onChanged: (bool value) {
                            setState(() {
                              _notificationsEnabled = value;
                            });
                          },
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 0.2,
                        color: Palette.silverChalice,
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      SettingsItem(
                        icon: Image.asset('assets/images/signout-icon.png'),
                        text: 'Sign out',
                        hasArrow: false,
                      ),
                      SizedBox(height: ScreenUtil().setHeight(33).toDouble()),
                      Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(30).toDouble(),
                        ),
                        child: SecPrimaryButton(
                          text: 'Save changes',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
