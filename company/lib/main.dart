import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:security_company_app/app/common/helpers/simple_bloc_delegate.dart';
import 'package:security_company_app/app/common/models/sec_app.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/security_app/screens/security_app.dart';
import 'package:security_company_app/app/signin/bloc/sign_in_bloc.dart';
import 'package:security_company_app/app/signin/data/signin_repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();

  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  await Hive.initFlutter();
  Hive.registerAdapter(SecAppAdapter());

  final signInRepository = SignInRepository();

  final appBloc = AppBloc();
  final signInBloc = SignInBloc(signInRepository: signInRepository);

  appBloc.add(AppLaunched());

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AppBloc>(
          create: (_) => appBloc,
        ),
        BlocProvider<SignInBloc>(
          create: (_) => signInBloc,
        ),
      ],
      child: SecurityApp(),
    ),
  );
}
