import 'package:flutter/material.dart';
import 'package:security_company_app/app/routes/router.gr.dart';

class SecurityApp extends StatefulWidget {
  @override
  _SecurityAppState createState() => _SecurityAppState();
}

class _SecurityAppState extends State<SecurityApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Security',
      onGenerateRoute: Router.onGenerateRoute,
      navigatorKey: Router.navigatorKey,
    );
  }
}
