import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/models/door_supervision_sub_service.dart';
import 'package:security_company_app/app/common/models/guard_type.dart';
import 'package:security_company_app/app/common/models/manned_security_sub_service.dart';
import 'package:security_company_app/app/common/models/order.dart';
import 'package:security_company_app/app/common/models/order_status.dart';
import 'package:security_company_app/app/common/models/static_security_sub_service.dart';
import 'package:security_company_app/app/common/widgets/avatar.dart';
import 'package:security_company_app/app/common/widgets/sec_navigation_bar.dart';
import 'package:security_company_app/app/dashboard/widgets/sec_filter_chip.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class ClientDashboardScreen extends StatefulWidget {
  ClientDashboardScreen({Key key}) : super(key: key);

  @override
  _ClientDashboardScreenState createState() => _ClientDashboardScreenState();
}

class _ClientDashboardScreenState extends State<ClientDashboardScreen> {
  OrderStatus _statusFilter;
  List<Order> _orders;

  @override
  void initState() {
    super.initState();

    _orders = [
      Order(
        id: 'SG0001',
        status: OrderStatus.done,
        company: Company(
          id: '1',
          name: 'ThunderSec',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 27, 12, 30, 43),
        dateFrom: DateTime(2020, 2, 28, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.securityGuard,
        guardsCount: 2,
        service: MannedSecuritySubService(),
        price: 120,
      ),
      Order(
        id: 'BG0003',
        status: OrderStatus.pending,
        bodyguard: Bodyguard(
          id: '5',
          name: 'Brian Norton',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 28, 12, 30, 43),
        dateFrom: DateTime(2020, 2, 28, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.bodyGuard,
        guardsCount: 1,
        service: StaticSecuritySubService(),
        price: 80,
      ),
      Order(
        id: 'SG0005',
        status: OrderStatus.pending,
        company: Company(
          id: '1',
          name: 'ThunderSec',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 27, 12, 30, 43),
        dateFrom: DateTime(2020, 3, 2, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.securityGuard,
        guardsCount: 2,
        service: DoorSupervisionSubService(),
        price: 120,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/dashboard-background.png'),
                    fit: BoxFit.cover,
                    alignment: Alignment.topRight,
                  ),
                  color: Palette.mystic,
                ),
                child: SafeArea(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setHeight(37).toDouble()),
                      Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(29).toDouble(),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  '£1,095.00',
                                  style: theme.clientPaidSumStyle,
                                ),
                                SizedBox(
                                    height: ScreenUtil().setHeight(7).toDouble()),
                                Text(
                                  'Total orders paid',
                                  style: theme.clientTotalOrdersStyle,
                                ),
                              ],
                            ),
                            InkWell(
                              onTap: () {
                                Router.navigator.pushNamed(Router.settingsScreen);
                              },
                              child: Avatar(
                                radius: ScreenUtil().setWidth(58).toDouble() / 2,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(31).toDouble()),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(8).toDouble(),
                          ),
                          padding: EdgeInsetsDirectional.only(
                            top: ScreenUtil().setHeight(20).toDouble(),
                            end: ScreenUtil().setWidth(9).toDouble(),
                            start: ScreenUtil().setWidth(9).toDouble(),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Palette.mystic,
                          ),
                          child: Column(
                            children: <Widget>[
                              Builder(
                                builder: (context) {
                                  final statusFilters = [
                                    {
                                      'filter': null,
                                      'text': 'All',
                                    },
                                    {
                                      'filter': OrderStatus.canceled,
                                      'text': 'Canceled',
                                    },
                                    {
                                      'filter': OrderStatus.done,
                                      'text': 'Done',
                                    },
                                  ];

                                  return Wrap(
                                    spacing: ScreenUtil().setWidth(8).toDouble(),
                                    runSpacing:
                                        ScreenUtil().setHeight(6).toDouble(),
                                    alignment: WrapAlignment.center,
                                    children: <Widget>[
                                      for (var filterData in statusFilters)
                                        Builder(
                                          builder: (context) {
                                            final filter =
                                                filterData['filter'] as OrderStatus;
                                            final text =
                                                filterData['text'] as String;
                                            final selected =
                                                _statusFilter == filter;

                                            return SecFilterChip(
                                              onTap: () {
                                                setState(() {
                                                  _statusFilter = filter;
                                                });
                                              },
                                              selected: selected,
                                              child: Text(
                                                text,
                                                style: selected
                                                    ? theme.filterTextStyle
                                                        .copyWith(
                                                        color: Colors.white,
                                                      )
                                                    : theme.filterTextStyle,
                                              ),
                                            );
                                          },
                                        ),
                                    ],
                                  );
                                },
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(22).toDouble()),
                              Expanded(
                                child: Builder(
                                  builder: (context) {
                                    final ordersByDate = <DateTime, List<Order>>{};
                                    final ordersFiltered = _orders.where((order) =>
                                        _statusFilter == null ||
                                        order.status == _statusFilter);
                                    ordersFiltered.forEach((order) {
                                      final orderDate = order.dateFrom;
                                      final date =
                                          DateTime(orderDate.year, orderDate.month);

                                      if (!ordersByDate.containsKey(date)) {
                                        ordersByDate[date] = [];
                                      }

                                      ordersByDate[date].add(order);
                                    });

                                    final dates = ordersByDate.keys.toList();
                                    dates.sort(
                                        (date1, date2) => date1.compareTo(date2));

                                    print(ordersByDate);

                                    return ListView.separated(
                                      itemCount: dates.length,
                                      separatorBuilder: (_, __) => SizedBox(
                                          height: ScreenUtil()
                                              .setHeight(13)
                                              .toDouble()),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final date = dates[index];
                                        final orders = ordersByDate[date];

                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                horizontal: ScreenUtil()
                                                    .setWidth(21 - 9)
                                                    .toDouble(),
                                              ),
                                              child: Text(
                                                DateFormat('MMM y')
                                                    .format(date)
                                                    .toUpperCase(),
                                                style: theme.orderDateHead,
                                              ),
                                            ),
                                            SizedBox(
                                                height: ScreenUtil()
                                                    .setHeight(7)
                                                    .toDouble()),
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                horizontal: ScreenUtil()
                                                    .setWidth(14)
                                                    .toDouble(),
                                              ),
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                                color: Colors.white,
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.black
                                                        .withOpacity(0.08),
                                                    offset: Offset(0, 2),
                                                    blurRadius: 4,
                                                  ),
                                                ],
                                              ),
                                              child: Column(
                                                children: <Widget>[
                                                  for (var i = 0;
                                                      i < orders.length;
                                                      i++)
                                                    Builder(
                                                      builder: (context) {
                                                        final order = orders[i];

                                                        return Column(
                                                          children: <Widget>[
                                                            Container(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                horizontal:
                                                                    ScreenUtil()
                                                                        .setWidth(6)
                                                                        .toDouble(),
                                                                vertical:
                                                                    ScreenUtil()
                                                                        .setHeight(
                                                                            19)
                                                                        .toDouble(),
                                                              ),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Container(
                                                                    width: ScreenUtil()
                                                                        .setWidth(
                                                                            38)
                                                                        .toDouble(),
                                                                    height: ScreenUtil()
                                                                        .setHeight(
                                                                            38)
                                                                        .toDouble(),
                                                                    child:
                                                                        FittedBox(
                                                                      fit: BoxFit
                                                                          .contain,
                                                                      child: order
                                                                          .service
                                                                          .icon,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                      width: ScreenUtil()
                                                                          .setWidth(
                                                                              21)
                                                                          .toDouble()),
                                                                  Expanded(
                                                                    child: Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .center,
                                                                      children: <
                                                                          Widget>[
                                                                        Text(
                                                                          order
                                                                              .service
                                                                              .name,
                                                                          style: theme
                                                                              .orderCardServiceName,
                                                                        ),
                                                                        Text(
                                                                          order.company
                                                                                  ?.name ??
                                                                              order
                                                                                  .bodyguard
                                                                                  ?.name ??
                                                                              '',
                                                                          style: theme
                                                                              .orderCardCompanyName,
                                                                        ),
                                                                        SizedBox(
                                                                            height: ScreenUtil()
                                                                                .setHeight(7)
                                                                                .toDouble()),
                                                                        Text(
                                                                          DateFormat(
                                                                                  'EEEE d MMM y')
                                                                              .format(
                                                                                  order.dateFrom),
                                                                          style: theme
                                                                              .orderCardDate,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                      width: ScreenUtil()
                                                                          .setWidth(
                                                                              21)
                                                                          .toDouble()),
                                                                  Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                        '£${order.price}',
                                                                        style: theme
                                                                            .orderCardPrice,
                                                                      ),
                                                                      SizedBox(
                                                                          height:
                                                                              1),
                                                                      Builder(
                                                                        builder:
                                                                            (context) {
                                                                          final duration = order
                                                                              .dateTo
                                                                              .difference(
                                                                                  order.dateFrom)
                                                                              .inDays;

                                                                          return Text(
                                                                            duration
                                                                                .toString(),
                                                                            style: theme
                                                                                .orderCardDuration,
                                                                          );
                                                                        },
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            if (i <
                                                                orders.length - 1)
                                                              Container(
                                                                height: 0.2,
                                                                color: Palette
                                                                    .silverChalice,
                                                              ),
                                                          ],
                                                        );
                                                      },
                                                    ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              //
              //
              Positioned(
                bottom: 0,
                child: SecNavigationBar(),
              ),
            ],
          ),
        );
      },
    );
  }
}
