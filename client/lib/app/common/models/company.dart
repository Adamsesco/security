import 'package:security_company_app/app/common/models/language.dart';
import 'package:security_company_app/app/common/models/user.dart';

class Company extends User {
  @override
  String id;

  @override
  String name;

  @override
  String phoneNumber;

  @override
  String avatarUrl;

  @override
  Language language;

  @override
  bool available;

  @override
  bool notificationsEnabled;

  Company(
      {this.id,
      this.name,
      this.phoneNumber,
      this.avatarUrl,
      this.language,
      this.available,
      this.notificationsEnabled});

  @override
  factory Company.fromMap(Map<String, dynamic> map) {
    return Company();
  }

  @override
  factory Company.copyFrom(Company other) {
    return Company(
      id: other.id,
      name: other.name,
      phoneNumber: other.phoneNumber,
      avatarUrl: other.avatarUrl,
      language: other.language,
      available: other.available,
      notificationsEnabled: other.notificationsEnabled,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return null;
  }
}
