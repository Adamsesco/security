import 'package:security_company_app/app/common/models/language.dart';

abstract class User {
  String id;
  String email;
  String token;
  String name;
  String phoneNumber;
  String avatarUrl;
  Language language;
  bool available;
  bool notificationsEnabled;

  User();

  factory User.fromMap(Map<String, dynamic> map) {
    return null;
  }

  factory User.copyFrom(User other) {
    return null;
  }

  Map<String, dynamic> toMap();
}
