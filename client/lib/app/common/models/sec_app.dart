import 'package:hive/hive.dart';
import 'package:meta/meta.dart';

part 'sec_app.g.dart';

@HiveType(typeId: 0)
class SecApp extends HiveObject {
  @HiveField(0)
  int launchTimesCount;

  SecApp({@required this.launchTimesCount});

  factory SecApp.initial() {
    return SecApp(
      launchTimesCount: 0,
    );
  }

  factory SecApp.copyFrom(SecApp other) {
    return SecApp(
      launchTimesCount: other.launchTimesCount,
    );
  }
}
