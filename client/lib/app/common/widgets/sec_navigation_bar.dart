import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/routes/router.gr.dart';

class SecNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: ScreenUtil().setHeight(78 + 18).toDouble(),
      alignment: Alignment.center,
      padding: EdgeInsets.only(
        top: ScreenUtil().setHeight(18).toDouble(),
      ),
      decoration: BoxDecoration(
//        color: Palette.ebonyClay,
        image: DecorationImage(
          image: AssetImage('assets/images/navbar-background.png'),
          fit: BoxFit.fill,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          InkWell(
            child: Container(
              child: Image.asset(
                'assets/images/orders-icon.png',
                width: ScreenUtil().setWidth(20).toDouble(),
                height: ScreenUtil().setWidth(28).toDouble(),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Router.navigator.pushNamed(Router.locationScreen);
            },
            child: Container(
              child: Image.asset(
                'assets/images/search-icon2.png',
                width: ScreenUtil().setWidth(32).toDouble(),
                height: ScreenUtil().setWidth(22).toDouble(),
              ),
            ),
          ),
          InkWell(
            child: Transform.translate(
              offset: Offset(0, -10),
              child: Container(
                child: Image.asset(
                  'assets/images/shield-icon.png',
                  width: ScreenUtil().setWidth(34).toDouble(),
                  height: ScreenUtil().setWidth(35).toDouble(),
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Router.navigator.pushNamed(Router.notificationsScreen);
            },
            child: Container(
              child: Image.asset(
                'assets/images/notifications-icon2.png',
                width: ScreenUtil().setWidth(23).toDouble(),
                height: ScreenUtil().setWidth(22.96).toDouble(),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Router.navigator.pushNamed(Router.settingsScreen);
            },
            child: Container(
              child: Image.asset(
                'assets/images/settings-icon.png',
                width: ScreenUtil().setWidth(24).toDouble(),
                height: ScreenUtil().setWidth(24).toDouble(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

//class _NavigationBarClipper extends CustomClipper<Path> {
//  @override
//  Path getClip(Size size) {
//    final path = Path();
//
//    path.moveTo(0, 18);
//
//    path.lineTo((size.width - 94) / 2, 18);
//
//    path.arcToPoint(
//      Offset((size.width - 94) / 2 + 8, 18 - 6 / 2),
//      clockwise: false,
//      radius: Radius.circular(26 / 2),
//    );
//
//    path.arcToPoint(
//      Offset((size.width + 94) / 2 - 8, 18 - 6 / 2),
//      radius: Radius.circular((140) / 2),
//    );
//
////    path.moveTo(0, ScreenUtil().setHeight(18).toDouble());
////
////    path.lineTo(ScreenUtil().setWidth(141).toDouble(),
////        ScreenUtil().setHeight(18).toDouble());
////
////    path.arcToPoint(
////      Offset(ScreenUtil().setWidth(141 + (12 * 2 / 3)).toDouble(),
////          ScreenUtil().setHeight(18 - 6 / 2).toDouble()),
////      clockwise: false,
////      radius: Radius.circular(ScreenUtil().setWidth(26 / 2).toDouble()),
////    );
////
////    path.arcToPoint(
////      Offset(ScreenUtil().setWidth(223 + (12 / 3)).toDouble(),
////          ScreenUtil().setHeight(18 - 6 / 2).toDouble()),
////      radius: Radius.circular(ScreenUtil().setWidth((140) / 2).toDouble()),
////    );
////    path.conicTo(
////      size.width / 2,
////      0,
////      ScreenUtil().setWidth(223 + (12 / 3)).toDouble(),
////      ScreenUtil().setHeight(18 - 6 / 2).toDouble(),
////      2,
////    );
//
//    path.arcToPoint(
//      Offset((size.width + 94) / 2, 18),
//      clockwise: false,
//      radius: Radius.circular(26 / 2),
//    );
//
//    path.lineTo(size.width, 18);
//    path.lineTo(size.width, size.height);
//    path.lineTo(0, size.height);
//    path.close();
//
//    return path;
//  }
//
//  @override
//  bool shouldReclip(CustomClipper<Path> oldClipper) {
//    return true;
//  }
//}
