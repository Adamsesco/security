import 'dart:io';

import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class Avatar extends StatelessWidget {
  final double radius;
  final bool hasBorder;
  final String url;
  final File file;

  Avatar({this.radius = 0, this.hasBorder = true, this.url, this.file});

  @override
  Widget build(BuildContext context) {
    Widget image;
    if (url != null) {
      image = CachedNetworkImage(
        imageUrl: url,
        fit: BoxFit.cover,
      );
    } else if (file != null) {
      image = Image.file(
        file,
        fit: BoxFit.cover,
      );
    } else {
      image = Image.asset(
        'assets/images/default-avatar.png',
        fit: BoxFit.cover,
      );
    }

    return Container(
      width: radius * 2,
      height: radius * 2,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: hasBorder
            ? Border.all(
                width: 2,
                color: Colors.white,
              )
            : null,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.50),
            offset: Offset(0, 1),
            blurRadius: 6,
            spreadRadius: -2,
          ),
        ],
      ),
      child: image,
    );
  }
}
