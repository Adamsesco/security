import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/notification.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class NotificationCard extends StatelessWidget {
  final SecNotification notification;

  NotificationCard({@required this.notification})
      : assert(notification != null);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Container(
          padding: EdgeInsetsDirectional.only(
            top: ScreenUtil().setHeight(22).toDouble(),
            end: ScreenUtil().setWidth(23).toDouble(),
            bottom: ScreenUtil().setHeight(17).toDouble(),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
          child: IntrinsicHeight(
            child: Row(
              children: <Widget>[
                Container(
                  width: 4,
                  margin: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(4).toDouble(),
                  ),
                  decoration: BoxDecoration(
                    color:
                        notification.read ? Palette.mischka : Palette.sanMarino,
                    borderRadius: BorderRadiusDirectional.only(
                      topEnd: Radius.circular(3),
                      bottomEnd: Radius.circular(3),
                    ),
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(32).toDouble()),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      notification.text,
                      style: theme.notificationTextStyle.copyWith(
                        color: notification.read
                            ? Palette.mischka
                            : Palette.luckyPoint,
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(12).toDouble()),
                    Text(
                      DateFormat('dd MMM y HH:mm').format(notification.date),
                      style: theme.notificationDateStyle.copyWith(
                        color: notification.read
                            ? Palette.mischka
                            : Palette.luckyPoint,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
