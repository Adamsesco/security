import 'dart:async';

import 'package:auto_route/auto_route_wrapper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/location/bloc/bloc.dart';

class LocationScreen extends StatefulWidget implements AutoRouteWrapper {
  @override
  Widget get wrappedRoute => BlocProvider<LocationBloc>(
        create: (_) => LocationBloc(),
        child: this,
      );

  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  LocationBloc _locationBloc;
  StreamSubscription _locationStateSubscription;
  final Completer<GoogleMapController> _mapController = Completer();
  final TextEditingController _addressController = TextEditingController();
  String _address;
  final Set<Marker> _markers = {};
  final _mapKey = UniqueKey();

  @override
  void initState() {
    super.initState();

    _locationBloc = context.bloc<LocationBloc>();
    _locationBloc.add(LocationRequested());
    _locationStateSubscription = _locationBloc.listen((state) {
      if (state is LocationUpdateSuccess) {
        final location = state.location;
        final address = state.address;

        _markers.clear();
        final marker = Marker(
          markerId: MarkerId('${location.latitude},${location.longitude}'),
          position: LatLng(location.latitude, location.longitude),
        );

        setState(() {
          _animateCamera(location);
          _markers.add(marker);
          _addressController.text = address ?? '';
          _address = address;
        });
      }
    });
  }

  Future<void> _animateCamera(Position location) async {
    if (_mapController == null || !_mapController.isCompleted) {
      return;
    }

    final controller = await _mapController.future;
    await controller.animateCamera(
        CameraUpdate.newLatLng(LatLng(location.latitude, location.longitude)));
    setState(() {});
  }

  @override
  void dispose() {
    _locationStateSubscription?.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationBloc, LocationState>(
        builder: (context, locationState) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            Expanded(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  GoogleMap(
                    key: _mapKey,
                    mapType: MapType.normal,
                    onMapCreated: (controller) {
                      _mapController.complete(controller);
                    },
                    onTap: (position) {
                      _locationBloc.add(LocationChanged(
                          location: Position(
                              latitude: position.latitude,
                              longitude: position.longitude)));
                    },
                    initialCameraPosition: CameraPosition(
                      target: LatLng(51.5283063, -0.3824686),
                      zoom: 18,
                    ),
                    markers: _markers,
                  ),
                  Positioned.directional(
                    textDirection: Directionality.of(context),
                    bottom: ScreenUtil().setHeight(18).toDouble(),
                    end: ScreenUtil().setWidth(13).toDouble(),
                    child: InkWell(
                      onTap: () {
                        _locationBloc.add(LocationRequested());
                      },
                      child: Container(
                        padding: EdgeInsets.all(
                            ScreenUtil().setWidth(14).toDouble()),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.07),
                              blurRadius: 6,
                              spreadRadius: 2,
                            ),
                          ],
                        ),
                        child: locationState is LocationLoadInProgress
                            ? CupertinoActivityIndicator()
                            : Image.asset(
                                'assets/images/my-location-button-icon.png',
                                width: ScreenUtil().setWidth(24).toDouble(),
                                height: ScreenUtil().setWidth(24).toDouble(),
                              ),
                      ),
                    ),
                  ),
                  Positioned.directional(
                    top: MediaQuery.of(context).padding.top +
                        ScreenUtil().setHeight(8.46).toDouble(),
                    start: ScreenUtil().setWidth(15).toDouble(),
                    textDirection: Directionality.of(context),
                    child: SecBackButton(),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(36).toDouble(),
                right: ScreenUtil().setWidth(30).toDouble(),
                bottom: ScreenUtil().setHeight(42).toDouble(),
                left: ScreenUtil().setWidth(30).toDouble(),
              ),
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/location-icon.png',
                        width: ScreenUtil().setWidth(12).toDouble(),
                        height: ScreenUtil().setWidth(16).toDouble(),
                      ),
                      TextField(
                        controller: _addressController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsetsDirectional.only(
                            start: ScreenUtil().setWidth(14 + 12).toDouble(),
                            bottom: ScreenUtil().setHeight(38).toDouble(),
                          ),
                          hintText: 'Please type tour address',
                          hintStyle: TextStyle(
                            fontFamily: 'Rubik',
                            fontSize: ScreenUtil().setSp(14).toDouble(),
                            color: Color(0xFFB8BCC7),
                          ),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xFFD8D8D8),
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xFFD8D8D8),
                            ),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xFFD8D8D8),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(38).toDouble()),
                  SecPrimaryButton(
                    text: 'Deliver here',
                    enabled: _address != null && _address.isNotEmpty,
                    onTap: () {
                      Router.navigator.pushNamed(Router.selectServiceScreen);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
