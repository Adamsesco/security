import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';

abstract class LocationState extends Equatable {
  const LocationState();
}

class InitialLocationState extends LocationState {
  @override
  List<Object> get props => [];
}

class LocationLoadInProgress extends LocationState {
  @override
  List<Object> get props => [];
}

class LocationUpdateSuccess extends LocationState {
  final Position location;
  final String address;

  LocationUpdateSuccess({@required this.location, this.address});

  @override
  List<Object> get props => [location];
}

class LocationUpdateFailure extends LocationState {
  @override
  List<Object> get props => [];
}
