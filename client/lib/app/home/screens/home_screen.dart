import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/security_app/screens/splash_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    return BlocListener<AppBloc, AppState>(
      condition: (previousState, currentState) {
        return (previousState is AppInitial ||
                previousState is AppLoadInProgress) &&
            currentState is AppLoadSuccess;
      },
      listener: (context, appState) {
        if (appState is AppLoadSuccess) {
          final app = appState.app;

          if (app.launchTimesCount == 1) {
            Router.navigator.pushReplacementNamed(Router.introScreen);
          } else {
            Router.navigator.pushReplacementNamed(Router.mainScreen);
          }
        }
      },
      child: SplashScreen(),
    );
  }
}
