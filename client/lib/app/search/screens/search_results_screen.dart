import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/widgets/expandible_container.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_screen_subtitle.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/routes/router.gr.dart';

class SearchResultsScreen extends StatefulWidget {
  SearchResultsScreen({Key key}) : super(key: key);

  @override
  _SearchResultsScreenState createState() => _SearchResultsScreenState();
}

class _SearchResultsScreenState extends State<SearchResultsScreen> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topRight,
              ),
              color: Palette.mystic,
            ),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Align(
                            alignment: AlignmentDirectional.centerStart,
                            child: SecBackButton(),
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                              'LIST',
                              style: theme.screenTitleStyle,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: AlignmentDirectional.centerEnd,
                            child: InkWell(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      ScreenUtil().setWidth(11).toDouble(),
                                  vertical:
                                      ScreenUtil().setWidth(12).toDouble(),
                                ),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Palette.springWood,
                                ),
                                child: Image.asset(
                                  'assets/images/filters-icon.png',
                                  width: ScreenUtil().setWidth(23).toDouble(),
                                  height: ScreenUtil().setWidth(21).toDouble(),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(23).toDouble()),
                  SecScreenSubtitle(
                    subtitle: 'At vero eos censes',
                  ),
                  SizedBox(height: ScreenUtil().setHeight(15.32).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Text(
                      'At vero eos censes aut odit aut quid percipit aut rerum necessitatibus saepe.',
                      style: theme.bodyTextStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(16).toDouble()),
                  Expanded(
                    child: ListView.separated(
                      itemCount: 1,
                      separatorBuilder: (_, __) => SizedBox(
                          height: ScreenUtil().setHeight(10).toDouble()),
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              _expanded = !_expanded;
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(8).toDouble(),
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Palette.springWood,
                              boxShadow: [
                                BoxShadow(
                                  color: Palette.sanMarino.withOpacity(0.22),
                                  offset: Offset(1, 1),
                                  blurRadius: 3,
                                  spreadRadius: -1,
                                ),
                              ],
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsetsDirectional.only(
                                      top:
                                          ScreenUtil().setHeight(16).toDouble(),
                                      end: ScreenUtil().setWidth(32).toDouble(),
                                      bottom:
                                          ScreenUtil().setHeight(13).toDouble(),
                                      start:
                                          ScreenUtil().setWidth(24).toDouble(),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Thundersec'.toUpperCase(),
                                              style: theme.companyNameStyle,
                                            ),
                                            SizedBox(
                                                height: ScreenUtil()
                                                    .setHeight(5)
                                                    .toDouble()),
                                            Row(
                                              children: <Widget>[
                                                Image.asset(
                                                  'assets/images/location-icon.png',
                                                  width: ScreenUtil()
                                                      .setWidth(10)
                                                      .toDouble(),
                                                  height: ScreenUtil()
                                                      .setWidth(14)
                                                      .toDouble(),
                                                  color: Palette.periwinkleGray,
                                                ),
                                                SizedBox(
                                                    width: ScreenUtil()
                                                        .setWidth(5)
                                                        .toDouble()),
                                                Text(
                                                  '2.5km',
                                                  style: theme.distanceStyle,
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                                height: ScreenUtil()
                                                    .setHeight(18)
                                                    .toDouble()),
                                            RichText(
                                              text: TextSpan(
                                                style: theme.priceStyle,
                                                text: '\$49',
                                                children: [
                                                  TextSpan(
                                                    text: '/Day',
                                                    style: theme.perDayStyle,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          width: ScreenUtil()
                                              .setWidth(65)
                                              .toDouble(),
                                          height: ScreenUtil()
                                              .setWidth(65)
                                              .toDouble(),
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                  'https://w0.pngwave.com/png/40/612/logo-security-company-guard-dog-graphic-design-warrior-helmet-png-clip-art.png'),
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  ExpandibleContainer(
                                    expanded: _expanded,
                                    duration: const Duration(milliseconds: 400),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(
                                            height: ScreenUtil()
                                                .setHeight(32 - 13)
                                                .toDouble()),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                            horizontal: ScreenUtil()
                                                .setWidth(30)
                                                .toDouble(),
                                          ),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'Order ID',
                                                      style: theme
                                                          .resultDetailsTitleStyle,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      'SG7862',
                                                      style: theme
                                                          .resultDetailsContentStyle,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(7)
                                                      .toDouble()),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'Order type',
                                                      style: theme
                                                          .resultDetailsTitleStyle,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      'Security Guard',
                                                      style: theme
                                                          .resultDetailsContentStyle,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(7)
                                                      .toDouble()),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'Order mission',
                                                      style: theme
                                                          .resultDetailsTitleStyle,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      'Door Supervision',
                                                      style: theme
                                                          .resultDetailsContentStyle,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(7)
                                                      .toDouble()),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'Nº of Guards',
                                                      style: theme
                                                          .resultDetailsTitleStyle,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      '3',
                                                      style: theme
                                                          .resultDetailsContentStyle,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(7)
                                                      .toDouble()),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'From',
                                                      style: theme
                                                          .resultDetailsTitleStyle,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      '17 Jan 2020',
                                                      style: theme
                                                          .resultDetailsContentStyle,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(7)
                                                      .toDouble()),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'To',
                                                      style: theme
                                                          .resultDetailsTitleStyle,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      '26 Jan 2020',
                                                      style: theme
                                                          .resultDetailsContentStyle,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(7)
                                                      .toDouble()),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'Address',
                                                      style: theme
                                                          .resultDetailsTitleStyle,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      'B63 Queens Boulevard London',
                                                      style: theme
                                                          .resultDetailsContentStyle,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                            height: ScreenUtil()
                                                .setHeight(42)
                                                .toDouble()),
                                        InkWell(
                                          onTap: () {
                                            print('Pay now');
                                            Router.navigator
                                                .pushNamed(Router.signinScreen);
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Palette.sanMarino,
                                            ),
                                            padding: EdgeInsetsDirectional.only(
                                              top: ScreenUtil()
                                                  .setHeight(22)
                                                  .toDouble(),
                                              end: ScreenUtil()
                                                  .setWidth(21)
                                                  .toDouble(),
                                              bottom: ScreenUtil()
                                                  .setHeight(20)
                                                  .toDouble(),
                                              start: ScreenUtil()
                                                  .setWidth(21)
                                                  .toDouble(),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                RichText(
                                                  text: TextSpan(
                                                    style:
                                                        theme.totalOrderStyle,
                                                    text: 'Total order  ',
                                                    children: [
                                                      TextSpan(
                                                        text: '\$441',
                                                        style:
                                                            theme.payPriceStyle,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Text(
                                                  'Pay now',
                                                  style: theme.payNowStyle,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
