import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  String _firstName;
  String _lastName;
  String _password;
  String _email;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
        builder: (BuildContext context, AppState state) {
      final appState = state as AppLoadSuccess;

      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Palette.sanMarino, Palette.cello],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                Row(
                  children: <Widget>[
                    SizedBox(width: ScreenUtil().setWidth(29).toDouble()),
                    SecBackButton(),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(19).toDouble()),
                Padding(
                  padding: EdgeInsetsDirectional.only(
                    start: ScreenUtil().setWidth(39).toDouble(),
                  ),
                  child: Text(
                    'Create new account',
                    style: appState.theme.titleStyle,
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(28).toDouble()),
                Expanded(
                  child: Container(
                    constraints: BoxConstraints(
                      minWidth: double.infinity,
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(31).toDouble(),
                      vertical: ScreenUtil().setHeight(34).toDouble(),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: AlignmentDirectional.topStart,
                          child: Text(
                            'Welcome',
                            style: appState.theme.headStyle,
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(5).toDouble()),
                        Align(
                          alignment: AlignmentDirectional.topStart,
                          child: Text(
                            'Sign up to continue',
                            style: appState.theme.subheadStyle,
                          ),
                        ),
                        Spacer(),
                        TextField(
                          onChanged: (String value) {
                            _firstName = value;
                          },
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(30.5).toDouble(),
                            ),
                            hintText: 'First name',
                            hintStyle: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: ScreenUtil().setSp(14).toDouble(),
                              color: Color(0xFFB8BCC7),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: ScreenUtil().setHeight(30.5).toDouble()),
                        TextField(
                          onChanged: (String value) {
                            _lastName = value;
                          },
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(30.5).toDouble(),
                            ),
                            hintText: 'Last name',
                            hintStyle: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: ScreenUtil().setSp(14).toDouble(),
                              color: Color(0xFFB8BCC7),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: ScreenUtil().setHeight(30.5).toDouble()),
                        TextField(
                          onChanged: (String value) {
                            _password = value;
                          },
                          obscureText: true,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(30.5).toDouble(),
                            ),
                            hintText: 'Password',
                            hintStyle: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: ScreenUtil().setSp(14).toDouble(),
                              color: Color(0xFFB8BCC7),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: ScreenUtil().setHeight(30.5).toDouble()),
                        TextField(
                          onChanged: (String value) {
                            _email = value;
                          },
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                              bottom: ScreenUtil().setHeight(30.5).toDouble(),
                            ),
                            hintText: 'Email address',
                            hintStyle: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: ScreenUtil().setSp(14).toDouble(),
                              color: Color(0xFFB8BCC7),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFD8D8D8),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        SizedBox(
                          width: ScreenUtil().setWidth(248).toDouble(),
                          child: Text(
                            'By creating an account, you agree to our terms & conditions',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: ScreenUtil().setSp(13).toDouble(),
                              color: Color(0xFFB8BCC7),
                            ),
                          ),
                        ),
                        Spacer(),
                        SecPrimaryButton(
                          color: Color(0xFF4361BA),
                          text: 'Sign up',
                        ),
                        Spacer(),
                        InkWell(
                          onTap: () {
                            Router.navigator.pushNamed(Router.signinScreen);
                          },
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: ScreenUtil().setSp(13).toDouble(),
                                color: Color(0xFFB8BCC7),
                              ),
                              text: 'Already have an account ? ',
                              children: [
                                TextSpan(
                                  text: 'Sign in',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFF4361BA),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
